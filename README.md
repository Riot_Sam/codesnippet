# README #

# Репозиторий состоит из: #
1. папка sign_in - содержит Fragment и необходимые части для MVP presenter'а (сам презентер и контракт, содержащий интерфейсы для вью и презентера) и тд;
2. папка account - содержит небольшой синглтон для доступа к сервер, использую Retrofit
3. верстку fragment'а sign_in
4. скриншот этого самого фрагмента для наглядности

Название пакета заменила на some_package, так как NDA

В проекте используется DataBinding как стандартная замена ButterKnife

MVP реализуется при помощи архитектурного фреймворка Mosby

# В проекте используется следующий перечень библиотек: #
	compile "com.android.support:appcompat-v7:$rootProject.supportLibraryVersion"
    compile "com.android.support:design:$rootProject.supportLibraryVersion"
    compile "com.android.support:support-v4:$rootProject.supportLibraryVersion"
    compile "com.android.support:cardview-v7:$rootProject.supportLibraryVersion"
    compile "com.android.support:recyclerview-v7:$rootProject.supportLibraryVersion"

    compile "com.google.android.gms:play-services-places:$rootProject.playServicesVersion"
    compile "com.google.android.gms:play-services-maps:$rootProject.playServicesVersion"

    debugCompile "com.squareup.leakcanary:leakcanary-android:$rootProject.leakCanaryVersion"
    releaseCompile "com.squareup.leakcanary:leakcanary-android-no-op:$rootProject.leakCanaryVersion"
    testCompile "com.squareup.leakcanary:leakcanary-android-no-op:$rootProject.leakCanaryVersion"

    // Retrofit
    compile "com.squareup.retrofit2:retrofit:$rootProject.retrofitVersion"
    compile "com.squareup.retrofit2:converter-gson:$rootProject.retrofitVersion"
    compile "com.squareup.okhttp3:okhttp:$rootProject.okHttpVersion"
    compile "com.squareup.okhttp3:logging-interceptor:$rootProject.okHttpVersion"

    compile "com.google.maps.android:android-maps-utils:0.4.3"
    compile "com.android.support.constraint:constraint-layout:1.0.2"
    compile "uk.co.chrisjenx:calligraphy:2.2.0"
    compile "com.github.bumptech.glide:glide:3.7.0"
    compile "com.crystal:crystalrangeseekbar:1.1.1"
    compile "org.greenrobot:eventbus:3.0.0"
    compile "com.hannesdorfmann.mosby3:mvp:3.0.4"