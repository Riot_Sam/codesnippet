package some_package.features.intro.sign_in;

import android.support.annotation.NonNull;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface SignInPageContract {

    interface View extends MvpView {
        void showSignUpView();

        void showResetPasswordView();

        void showProgress(boolean show);

        void showValidationError(SignInPageField field, String error);

        void showSignInError(Throwable error);

        void onSignInComplete();
    }

    interface Presenter extends MvpPresenter<View> {
        void switchToRegisterFlow();

        void recoverPassword();

        void signIn(@NonNull String email, @NonNull String password);

        void signInWithFacebook();

        void signInWithGoogle();
    }
}
