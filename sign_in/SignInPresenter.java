package some_package.features.intro.sign_in;

import android.support.annotation.NonNull;
import some_package.data.account.AccountManager;
import some_package.objects.User;
import some_package.utils.StringUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.io.IOException;
import java.util.Objects;

import static some_package.Constants.MIN_PASSWORD_LENGTH;

public class SignInPresenter extends MvpBasePresenter<SignInPageContract.View>
        implements SignInPageContract.Presenter {

    private AccountManager mAccountManager;

    public SignInPresenter(AccountManager accountManager) {
        this.mAccountManager = accountManager;
    }

    /* SignInPageContract.Presenter */

    @Override
    public void signIn(@NonNull String email, @NonNull String password) {
        Objects.requireNonNull(email);
        Objects.requireNonNull(password);

        if (!isViewAttached()) {
            return;
        }

        boolean validationFailed = true;
        if (!StringUtils.isValidEmail(email)) {
            getView().showValidationError(SignInPageField.EMAIL, "Email is not valid");
        } else if (StringUtils.isEmpty(password) || password.length() < MIN_PASSWORD_LENGTH) {
            getView().showValidationError(SignInPageField.PASSWORD, "Password is too short");
        } else {
            validationFailed = false;
        }

        if (!validationFailed) {
            getView().showProgress(true);
            mAccountManager.signIn(email, password, new AccountManager.AccessAccountCallback() {
                @Override
                public void onAccessGranted(User user) {
                    if (isViewAttached()) {
                        getView().showProgress(false);
                        getView().onSignInComplete();
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    if (isViewAttached()) {
                        String s;
                        if (throwable instanceof IOException) {
                            s = "Unable to sign-in, please check your Network settings.";
                        } else {
                            s = String.format("Unable to sign in (%s)", throwable.getMessage());
                        }

                        getView().showSignInError(new Throwable(s));
                        getView().showProgress(false);
                    }
                }
            });
        }
    }

    @Override
    public void signInWithFacebook() {
        if (!isViewAttached()) {
            return;
        }

        getView().showProgress(true);
        mAccountManager.signInWithFacebook(new AccountManager.AccessAccountCallback() {
            @Override
            public void onAccessGranted(User user) {
                if (isViewAttached()) {
                    getView().showProgress(false);
                    getView().onSignInComplete();
                }
            }

            @Override
            public void onError(Throwable throwable) {
                if (isViewAttached()) {
                    getView().showProgress(false);
                    getView().showSignInError(throwable);
                }
            }
        });
    }

    @Override
    public void signInWithGoogle() {
        if (!isViewAttached()) {
            return;
        }

        getView().showProgress(true);
        mAccountManager.signInWithGoogle(new AccountManager.AccessAccountCallback() {
            @Override
            public void onAccessGranted(User user) {
                if (isViewAttached()) {
                    getView().showProgress(false);
                    getView().onSignInComplete();
                }
            }

            @Override
            public void onError(Throwable throwable) {
                if (isViewAttached()) {
                    getView().showProgress(false);
                    getView().showSignInError(throwable);
                }
            }
        });
    }

    @Override
    public void switchToRegisterFlow() {
        if (isViewAttached()) {
            getView().showSignUpView();
        }
    }

    @Override
    public void recoverPassword() {
        if (isViewAttached()) {
            getView().showResetPasswordView();
        }
    }
}
