package some_package.features.intro.sign_in;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import some_package.R;
import some_package.base.TextWatcherAdapter;
import some_package.databinding.FragmentSignInBinding;
import some_package.data.account.ErrandAccountManager;
import some_package.features.intro.events.ResetPasswordRequestedEvent;
import some_package.features.intro.events.SignUpRequestedEvent;
import some_package.features.intro.events.UserSignedInEvent;
import some_package.utils.Logger;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import org.greenrobot.eventbus.EventBus;

public class SignInFragment extends MvpFragment<SignInPageContract.View, SignInPageContract.Presenter>
        implements SignInViewHandlers, SignInPageContract.View {

    private FragmentSignInBinding mBinding;

    private EditTextChangedListener mEmailEditTextListener;
    private EditTextChangedListener mPasswordEditTextListener;

    private Logger mLogger;

    public SignInFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public SignInPageContract.Presenter createPresenter() {
        return new SignInPresenter(ErrandAccountManager.getInstance());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLogger = Logger.withClass(getClass());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentSignInBinding.inflate(inflater, container, false);
        mBinding.setClickHandlers(this);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mBinding.emailTextInputLayout.getEditText().removeTextChangedListener(emailEditTextListener);
        mBinding.passwordTextInputLayout.getEditText().removeTextChangedListener(passwordEditTextListener);

        mEmailEditTextListener = null;
        mPasswordEditTextListener = null;
    }

    /* SignInViewHandlers */

    @Override
    public void onSignInButtonClicked(View view) {
        String email = getEmail();
        String password = getPassword();
        getPresenter().signIn(email, password);
    }

    @Override
    public void onSignUpButtonClicked(View view) {
        getPresenter().switchToRegisterFlow();
    }

    @Override
    public void onFacebookButtonClicked(View view) {
        getPresenter().signInWithFacebook();
    }

    @Override
    public void onGooglePlusButtonClicked(View view) {
        getPresenter().signInWithGoogle();
    }

    @Override
    public void onForgotPasswordButtonClicked(View view) {
        getPresenter().recoverPassword();
    }

    /* SignInPageContract.View */

    @Override
    public void showSignUpView() {
        EventBus.getDefault().post(new SignUpRequestedEvent());
    }

    @Override
    public void showResetPasswordView() {
        EventBus.getDefault().post(new ResetPasswordRequestedEvent());
    }

    @Override
    public void showProgress(boolean show) {
        mBinding.logoWithProgressView.showProgressBar(show);
    }

    @Override
    public void showValidationError(SignInPageField field, String error) {
        mLogger.logDebug(String.format("showValidationError: field=%s, error=%s", field, error));
        Resources res = getResources();
        switch (field) {
            case EMAIL:
                binding.emailTextInputLayout.setError(res.getString(R.string.invalid_email_error));
                break;

            case PASSWORD:
                binding.passwordTextInputLayout.setError(res.getString(R.string.password_too_short_error));
                break;

            default:
                logger.logError("showValidationError: unknown field = " + field);
                break;
        }
    }

    @Override
    public void showSignInError(Throwable error) {
        mLogger.logError("showSignInError", error);
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSignInComplete() {
        EventBus.getDefault().post(new UserSignedInEvent());
    }

    /* Private methods */

    private void setupUI() {
        mBinding.signInButton.setEnabled(false); // disabled by default

        mEmailEditTextListener = new EditTextChangedListener(mBinding.emailTextInputLayout);
        mPasswordEditTextListener = new EditTextChangedListener(mBinding.passwordTextInputLayout);

        EditText emailEditText = mBinding.emailTextInputLayout.getEditText();
        EditText passwordEditText = mBinding.passwordTextInputLayout.getEditText();
        emailEditText.addTextChangedListener(emailEditTextListener);
        passwordEditText.addTextChangedListener(passwordEditTextListener);

        mBinding.socialLoginView.getFacebookTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFacebookButtonClicked(v);
            }
        });

        mBinding.socialLoginView.getGoogleTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGooglePlusButtonClicked(v);
            }
        });
    }

    private String getEmail() {
        EditText editText = mBinding.emailTextInputLayout.getEditText();
        return editText.getText().toString();
    }

    private String getPassword() {
        EditText editText = mBinding.passwordTextInputLayout.getEditText();
        return editText.getText().toString();
    }

    private boolean allTextFieldsFilled() {
        return (!getEmail().isEmpty() && !getPassword().isEmpty());
    }

    private final class EditTextChangedListener extends TextWatcherAdapter {

        TextInputLayout mTextInputLayout;

        public EditTextChangedListener(TextInputLayout textInputLayout) {
            this.mTextInputLayout = textInputLayout;
        }

        @Override
        public void afterTextChanged(Editable s) {
            boolean enabled = allTextFieldsFilled();
            mBinding.signInButton.setEnabled(enabled);

            // Hide the error view
            mTextInputLayout.setErrorEnabled(false);
        }
    }
}
