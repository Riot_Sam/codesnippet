package some_package.features.intro.sign_in;

public enum SignInPageField {
    EMAIL,
    PASSWORD
}
