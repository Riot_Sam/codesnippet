package some_package.features.intro.sign_in;

import android.view.View;
import some_package.features.intro.sign_up.SignUpViewHandlers;

public interface SignInViewHandlers extends SignUpViewHandlers {

    void onForgotPasswordButtonClicked(View view);
}
