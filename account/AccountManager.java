package some_package.data.account;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import some_package.objects.User;

public interface AccountManager {

    interface AccessAccountCallback {
        void onAccessGranted(User user);

        void onError(Throwable throwable);
    }

    interface ResetPasswordCallback {
        void onPasswordReset();

        void onError(Throwable throwable);
    }

    void createAccount(@NonNull String email,
                       @NonNull String phoneNumber,
                       @NonNull String password,
                       @NonNull AccessAccountCallback callback);

    void signIn(@NonNull String email,
                @NonNull String password,
                @NonNull AccessAccountCallback callback);

    void signInWithFacebook(@NonNull AccessAccountCallback callback);

    void signInWithGoogle(@NonNull AccessAccountCallback callback);

    void resetPassword(@NonNull String email, @NonNull ResetPasswordCallback callback);
}
