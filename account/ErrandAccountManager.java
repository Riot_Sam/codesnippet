package some_package.data.account;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import some_package.api.UsersApi;
import some_package.api.retrofit.RetrofitCallback;
import some_package.api.retrofit.RetrofitError;
import some_package.api.retrofit.RetrofitGenerator;
import some_package.features.intro.sign_up.SignUpErrorResponse;
import some_package.objects.User;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.util.Objects;

public class ErrandAccountManager implements AccountManager {

    private static ErrandAccountManager sInstance;

    private ErrandAccountManager() {
        // Singleton
    }

    public static synchronized ErrandAccountManager getInstance() {
        if (sInstance == null) {
            sInstance = new ErrandAccountManager();
        }
        return sInstance;
    }

    @Override
    public void createAccount(@NonNull String email,
                              @NonNull String phoneNumber,
                              @NonNull String password,
                              @NonNull final AccessAccountCallback callback) {
        Objects.requireNonNull(email);
        Objects.requireNonNull(phoneNumber);
        Objects.requireNonNull(password);
        Objects.requireNonNull(callback);

	Retrofit retrofit = new Retrofit.Builder()
    	.baseUrl(URL_BASE)
    	.build();

	UsersApi service = retrofit.create(UsersApi.class);
        Call<User> call = service.signUp(email, phoneNumber, password);
        call.enqueue(new RetrofitCallback<User, SignUpErrorResponse>(SignUpErrorResponse.class, retrofit) {
            @Override
            public void onSuccess(Call<User> call, User user) {
                persistUser(user);
                callback.onAccessGranted(user);
            }

            @Override
            public void onFailure(Call<User> call, RetrofitError<SignUpErrorResponse> error) {
                callback.onError(error.hasErrorBody() ? error.getApiErrorBody() : error.getException());
            }
        });
    }

    @Override
    public void signIn(@NonNull String email,
                       @NonNull String password,
                       @NonNull final AccessAccountCallback callback) {
        Objects.requireNonNull(email);
        Objects.requireNonNull(password);
        Objects.requireNonNull(callback);

        Retrofit retrofit = new Retrofit.Builder()
    	.baseUrl(URL_BASE)
    	.build();

	UsersApi service = retrofit.create(UsersApi.class);
        Call<User> call = service.create(UsersApi.class).signIn(email, password);
        call.enqueue(new RetrofitCallback<User, Void>(Void.class, retrofit) {
            @Override
            public void onSuccess(Call<User> call, User user) {
                persistUser(user);
                callback.onAccessGranted(user);
            }

            @Override
            public void onFailure(Call<User> call, RetrofitError<Void> error) {
                callback.onError(error.getException());
            }
        });

    }

    @Override
    public void signInWithFacebook(@NonNull final AccessAccountCallback callback) {
        Objects.requireNonNull(callback);

        // ...
    }

    @Override
    public void signInWithGoogle(@NonNull final AccessAccountCallback callback) {
        Objects.requireNonNull(callback);

        // ...
    }

    @Override
    public void resetPassword(@NonNull String email, @NonNull final ResetPasswordCallback callback) {
        Objects.requireNonNull(email);
        Objects.requireNonNull(callback);

        Retrofit retrofit = new Retrofit.Builder()
    	.baseUrl(URL_BASE)
    	.build();

	UsersApi service = retrofit.create(UsersApi.class);
        Call<Void> call = service.create(UsersApi.class).recoverPassword(email);
        call.enqueue(new RetrofitCallback<Void, Void>(Void.class, retrofit) {
            @Override
            public void onSuccess(Call<Void> call, Void aVoid) {
                callback.onPasswordReset();
            }

            @Override
            public void onFailure(Call<Void> call, RetrofitError<Void> error) {
                callback.onError(error.getException());
            }
        });
    }
}
